#pragma once

#include <string>
#include <JsonBox.h>

#include "Item.h"

//Forward declaration
class EntityManager;

class Armor : public Item
{
public:
    int defense;

    //ctors
    Armor(std::string id, std::string name, std::string description, int defense);
    Armor(std::string id, JsonBox::Value& v, EntityManager* mgr);

    //Load the armor from the Json value
    void load(JsonBox::Value& v, EntityManager* mgr);
};
