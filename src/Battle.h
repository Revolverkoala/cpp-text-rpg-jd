#pragma once

#include <vector>
#include "Dialogue.h"

class Creature;

enum class BattleEventType {ATTACK, DEFEND};

class BattleEvent
{
public:
    //Creature that initiated event (attacker)
    Creature* source;
    //Creature being affected (target)
    Creature* target;
    //Type of event; attack, defense
    BattleEventType type;

    //ctor
    BattleEvent(Creature* source, Creature* target, BattleEventType type);

    //Convert event type to the corresponding function and call it on the source and target
    int run();
};

class Battle
{
private:
    //All the creatures participating.
    //We assume the player is a Creature with id "player".
    //A vector is used since we need to get the nth element
    //for use with a Dialogue
    std::vector<Creature*> combatants;

    //Actions the player can take in the battle
    Dialogue battleOptions;

    //Remove a creature from the combatants list, and report that it's dead
    void kill(Creature* creature);

    //Run the next turn for the enemies and the player.
    //Compute what the enemies should do and ask for the player's action, then compile
    //an event queue of the actions before proceeding through the queue
    //and running each action.
    void nextTurn();

public:
    //ctor
    Battle(std::vector<Creature*>& combatants);

    //Run the battle until the player dies or all the opposing
    //combatants do
    void run();
};
