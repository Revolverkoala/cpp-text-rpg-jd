#include <string>
#include <JsonBox.h>

#include "Entity.h"
#include "Item.h"
#include "EntityManager.h"

Item::Item(std::string id, std::string name, std::string description) : Entity(id)
{
    this->name = name;
    this->description = description;
}

Item::Item(std::string id, JsonBox::Value& v, EntityManager* mgr) : Entity(id)
{
    this->load(v, mgr);
}

//TODO: Add error checking
void Item::load(JsonBox::Value& v, EntityManager* mgr)
{
    JsonBox::Object o = v.getObject();
    this->name = o["name"].getString();
    this->description = o["description"].getString();
}
