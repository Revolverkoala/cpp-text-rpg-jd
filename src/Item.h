#pragma once

#include <string>
#include <JsonBox.h>

#include "Entity.h"

class EntityManager;

class Item : public Entity
{
public:

    //Name and description of item
    std::string name;
    std::string description;

    //ctors
    Item(std::string id, std::string name, std::string description);
    Item(std::string id, JsonBox::Value& v, EntityManager* mgr);

    //Load the item information from the JSON value
    virtual void load(JsonBox::Value& v, EntityManager* mgr);
};
