#include <string>
#include <JsonBox.h>

#include "Weapon.h"
#include "Item.h"
#include "EntityManager.h"

Weapon::Weapon(std::string id, std::string name, std::string description, int damage)
    : Item(id, name, description)
{
    this->damage = damage;
}

Weapon::Weapon(std::string id, JsonBox::Value& v, EntityManager* mgr)
    : Item(id, v, mgr)
{
    this->load(v, mgr);
}


//TODO: Add error checking
void Weapon::load(JsonBox::Value& v, EntityManager* mgr)
{
    JsonBox::Object o = v.getObject();
    this->damage = o["damage"].getInteger();
}
