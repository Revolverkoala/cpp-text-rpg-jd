#pragma once

#include <string>
#include <JsonBox.h>

#include "Item.h"

//Forward declaration
class EntityManager;

class Weapon : public Item
{
public:
    int damage;

    //ctors
    Weapon(std::string id, std::string name, std::string description, int damage);
    Weapon(std::string id, JsonBox::Value& v, EntityManager* mgr);

    //Load the armor from the Json value
    void load(JsonBox::Value& v, EntityManager* mgr);
};
