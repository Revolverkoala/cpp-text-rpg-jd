#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include <JsonBox.h>

#include "Item.h"
#include "Weapon.h"
#include "Armor.h"
#include "EntityManager.h"
#include "Dialogue.h"
#include "Player.h"
#include "Area.h"
#include "Door.h"
#include "Battle.h"

//Keeps track of items, weapons, creatures, etc
EntityManager entityManager;

Player startGame();
void dialogueMenu(Player& player);

int main()
{
    //Load entities
    entityManager.loadJson<Item>("Items.json");
    entityManager.loadJson<Weapon>("Weapons.json");
    entityManager.loadJson<Armor>("Armor.json");
    entityManager.loadJson<Creature>("Creatures.json");
    entityManager.loadJson<Door>("Doors.json");
    entityManager.loadJson<Area>("Areas.json");

    //Seed the rng with the system time
    std::srand(std::time(nullptr));

    Player player = startGame();

    //Set the current area to the first in the atlas
    player.currentArea = "area_01";

    while(true)
    {
        //Mark the current player as visited
        player.visitedAreas.insert(player.currentArea);

        //Pointer to the current area for convenience
        Area* areaPtr = player.getAreaPtr(&entityManager);

        //Autosave the game
        player.save(&entityManager);

        //If the area has any creatures in it, start a battle with them
        if (areaPtr->creatures.size() > 0)
        {
            //Create a vector of pointers to the creatures in the area
            std::vector<Creature*> combatants;
            std:: cout << "You are attacked by ";
            for(int i = 0; i < areaPtr->creatures.size(); ++i)
            {
                Creature* c = &(areaPtr->creatures[i]);
                combatants.push_back(c);
                std::cout << c ->name << (i == areaPtr->creatures.size()-1 ? "!\n" : ", ");
            }
            //Add the player to the combatant vector
            combatants.push_back(&player);
            //Run the battle
            Battle battle(combatants);
            battle.run();

            //If the player is still alive, grant them some xp,
            //assuming every creature was killed
            if(player.hp > 0)
            {
                //Or use std::accumulate, but that's an additional header
                unsigned int xp = 0;
                for (auto creature : areaPtr->creatures) xp += creature.xp;
                std::cout << "You gained " << xp << " experience!\n";
                player.xp += xp;
                //Remove the creatures from the area
                areaPtr->creatures.clear();
                //Restart the loop to force a save, then the game will carry on as usual
                continue;
            } //Otherwise player is dead, so end the program
            else
            {
                std::cout << "\t----YOU DIED----\n    Game over\n";
                return 0;
            }
        }
        //If the player has died, inform them and close
        if(player.hp <= 0)
        {
            std::cout << "\t----YOU DIED----\n    Game Over\n";
            return 0;
        }

        //Add search and movement options to dialogue
        Dialogue roomOptions = areaPtr->dialogue;

        for(auto door : areaPtr->doors)
        {
            roomOptions.addChoice("Go through the " + door->description);
        }

        roomOptions.addChoice("Search");

        //Activate the current area's dialogue
        int result = roomOptions.activate();

        if(result == 0)
        {
            dialogueMenu(player);
        } else if (result <= areaPtr->dialogue.size())
        {
            //Add more events here
        } else if (result < roomOptions.size())
        {
            Door* door = areaPtr->doors.at(result-areaPtr->dialogue.size()-1);
            int flag = player.traverse(door);

            switch (flag)
            {
            default:
            case 0:
                std::cout << "The " << door->description << " is locked." << std::endl;
                break;
            case 1:
                std::cout << "You unlock the " << door->description << " and go through it." << std::endl;
                break;
            case 2:
                std::cout << "You go through the " << door->description << "." << std::endl;
                break;
            }
        } else
        {
            std::cout << "You find: " << std::endl;
            areaPtr->items.print();
            player.inventory.merge(&(areaPtr->items));
            areaPtr->items.clear();
        }
    }

    return 0;
}

Player startGame()
{
    //Ask for a name and class
    //Name does not use a dialogue since dialogues only request options,
    //not string input. Could be generalized into its own TextInput class
    std::cout << "What's your name?" << std::endl;
    std::string name;
    std::cin >> name;

    //Check for existence then open using JsonBox if it exists
    std::ifstream f((name + ".json").c_str());
    if (f.good())
    {
        f.close();
        //Load the player
        JsonBox::Value saveData;
        JsonBox::Value areaData;
        saveData.loadFromFile(name + ".json");
        areaData.loadFromFile(name + "_areas.json");
        Player player = Player(saveData, areaData, &entityManager);

        return player;
    } else
    {
        f.close();
        int result = Dialogue(
              "Choose your class",
              {
                  "Fighter", "Rogue"
              }).activate();

        switch(result)
        {
        case 1:
            return Player(name, 15, 5, 4, 1.0/64.0, 0, 1, "Fighter");
        case 2:
            return Player(name, 15, 4, 5, 1.0/64.0, 0, 1, "Rogue");
        default:
            return Player(name, 15, 4, 4, 1.0/64.0, 0, 1, "Adventurer");
        }
    }
}

void dialogueMenu(Player& player)
{
    //Output the menu
    int result = Dialogue(
            "Menu\n====",
            {
                "Items", "Equipment", "Character"
            }).activate();

    switch (result)
    {
        //Print the items the player owns
    case 1:
        std::cout << "Items\n=====\n";
        player.inventory.print();
        std::cout << "----------------\n";
        break;
        //print the equipment the player is wearing (if any)
        //and then ask if they want to equip something
    case 2:
        {
        std::cout << "Equipment\n=========\n";
        std::cout << "Armor: "
            << (player.equippedArmor != nullptr ?
                player.equippedArmor->name : "Nothing")
            << std::endl;
        std::cout << "Weapon: "
            << (player.equippedWeapon != nullptr ?
                player.equippedWeapon->name : "Nothing")
            << std::endl;

        int result2 = Dialogue(
                "",
                {
                    "Equip Armor", "Equip Weapon", "Close"
                }).activate();

        //Equipping armor
        if(result2 == 1)
        {
            int userInput = 0;

            //Cannot equip armor if they don't have any
            //Print a list of armor and retrieve the amt
            //of armor in one go
            int numItems = player.inventory.print<Armor>(true);
            if (numItems == 0) break;

            while(!userInput)
            {
                //Choose a piece of armor to equip
                std::cout << "Equip which item?" << std::endl;
                std::cin >> userInput;
                //Equipment is numbered but is stored in a list, so
                //the number must be converted to a list element
                if(userInput >= 1 && userInput <= numItems)
                {
                    player.equipArmor(player.inventory.get<Armor>(userInput-1));
                }
            }
        } else if (result2 == 2)
        {
            int userInput = 0;
            int numItems = player.inventory.print<Weapon>(true);

            if (numItems == 0) break;

            while(!userInput)
            {
                std::cout << "Equip which item?" << std::endl;
                std::cin >> userInput;
                if(userInput >= 1 && userInput <= numItems)
                {
                    player.equipWeapon(player.inventory.get<Weapon>(userInput-1));
                }
            }
        }
        std::cout << "----------------\n";
        break;
        }
    //Output character info
    case 3:
        std::cout << "Character\n=========\n";
        std::cout << player.name;
        if(player.className != "") std::cout << " the " << player.className;
        std::cout << std::endl;

        std::cout << "Health:   " << player.hp << " / " << player.maxHp << std::endl;
        std::cout << "Strength: " << player.strength << std::endl;
        std::cout << "Agility:  " << player.agility << std::endl;
        std::cout << "Level:    " << player.level << " (" << player.xp;
        std::cout << " / " << player.xpToLevel(player.level+1) << ")" << std::endl;
        std::cout << "----------------\n";
        break;
    default:
        break;
    }
}
